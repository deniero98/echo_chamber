import pandas as pd
import numpy as np

from transformers import pipeline

#sentiment_analysis_multi_class = pipeline("sentiment-analysis", model="microsoft/deberta-base-mnli")
sentiment_analysis_pos_neg = pipeline("sentiment-analysis", model="distilbert-base-uncased-finetuned-sst-2-english", device=0)

def analyze_sentiment(df :pd.DataFrame, outname, main_column="title"):
    bodies = df

    #sentiment_score = np.empty(shape=(len(df), 1))
    #sentiment_score[:] = np.nan

    #sentiment_label = np.empty(shape=(len(df), 1), dtype=str)
    #sentiment_label[:] = np.nan
    sentiment_score_binary = np.empty(shape=(len(df), 1))
    sentiment_score_binary[:] = np.nan

    sentiment_label_binary = np.empty(shape=(len(df), 1), dtype=bool)
    sentiment_label_binary[:] = np.nan



    for idx, row in bodies[[main_column]].iterrows():
        # print(idx, row.values[0])
        try:
            cutoff = min(512, len(row.values[0]))

            # calc entailment/neutral/contradiction
            input = row.values[0][0:cutoff]
            # result = sentiment_analysis_multi_class(input)[0]
            # if idx % 100 == 0:
            #     print(idx, result, f"status: {idx}/{bodies.shape[0]}")
            # sentiment_label[idx, 0] = result["label"]
            # sentiment_score[idx, 0] = result["score"]
            # calc positive/negative
            result = sentiment_analysis_pos_neg(input)[0]
            if idx % 100 == 0:
                print(idx, result, f"status:  {idx}/{bodies.shape[0]}")
                print(input)
            sentiment_label_binary[idx, 0] = True if result["label"] == 'POSITIVE' else False
            sentiment_score_binary[idx, 0] = result["score"]
        except Exception as e:
            print(str(e))
            sentiment_label_binary[idx, 0] = "INVALID"
            sentiment_score_binary[idx, 0] = 0.0

    #bodies["sentiment_label"] = sentiment_label
    #bodies["sentiment_score"] = sentiment_score
    bodies["sentiment_label_binary"] = sentiment_label_binary
    bodies["sentiment_score_binary"] = sentiment_score_binary
    bodies.to_csv(f"data/{outname}", index = False)

df = pd.read_csv("data/Conservative_comments", error_bad_lines=False)
analyze_sentiment(df, "Conservative_comments_labeled", main_column="body")  # body only for comments
