import pandas as pd
from urllib.parse import urlparse
import operator

import matplotlib.pyplot as plt
import requests
from bs4 import BeautifulSoup


def create_pie_chart(link_dic, size, amount_break, name):
    # sort after biggest
    link_dic = sorted(link_dic.items(), key=operator.itemgetter(1), reverse=True)
    counter = 0
    amount = 0
    values = []
    labels = []
    legend = []
    print("[MESSAGE] We have " + str(len(link_dic)) + " different links in: " + str(size) + " submissions.")

    for key, value in link_dic:
        # percentage of how many individual slices there will be
        # 60 for republican submissions
        # 65 for democrats
        if amount >= amount_break:
            break
        amount = amount + (value / size) * 100
        values.append((value / size) * 100)
        labels.append(key)

        legend.append(str(key) + " : " + str(format((value / size) * 100, ".2f")) + "%")
        counter = counter + 1

    values.append(100 - amount)
    labels.append("others")
    legend.append(str("others") + " : " + str(format(100 - amount, ".2f")) + "%")

    # plot with labels when you want to see the labels preferable with fewer slices
    plt.pie(values, labels=labels)
    plt.title(name)
    plt.legend(legend, bbox_to_anchor=(-0.8, 1), loc="upper left")

    # plot with no labels
    # when you set the amount to 100 or you have too many slices print this plot
    plt.pie(values)

    plt.subplots_adjust(left=0.4)
    plt.show()
    print("[PLOT] plotted pie chart with media links")

    # this is important
    # array for media links which are most used in the specific subreddits
    anti_meme_array = ["www.reddit.com", "i.redd.it", "youtu.be", "v.redd.it", "www.youtube.com",
                       "imgur.com", "i.imgur.com"]
    new_size = size

    list_without_media = []
    for key, value in link_dic:
        if key in anti_meme_array:
            new_size = new_size - value
        else:
            list_without_media.append((key, value))

    anti_meme_plot(list_without_media, new_size, name)
    show_bias_of_news_links(list_without_media, name)


def anti_meme_plot(list, size, name):
    amount = 0
    values = []
    labels = []
    counter = 0
    print("[MESSAGE] We have " + str(len(list)) + " different links when we cut the most used media links.")
    print("[MESSAGE] So we have around this amount of news sites")

    for key, value in list:
        if amount >= 100:
            break
        amount = amount + (value / size) * 100
        values.append((value / size) * 100)
        if counter < 6:
            labels.append(key + ' ' + str(format(values[counter], '.0f')) + '%')
        else:
            labels.append('')

        counter = counter + 1

    plt.title(name)
    plt.pie(values, labels=labels)
    # plt.legend(legend, bbox_to_anchor=(-0.8,1), loc="upper left")
    # plt.subplots_adjust(left=0.4)
    plt.show()
    print("[PLOT] plotted pie chart without media links")


def extract_different_links(urls):
    counter = 0
    different_links_dic = dict()

    for url in urls:
        netloc = urlparse(url).netloc

        if netloc in different_links_dic:
            different_links_dic[netloc] = different_links_dic[netloc] + 1
        else:
            different_links_dic[netloc] = 1

        counter = counter + 1
    try:
        different_links_dic['non_media'] = different_links_dic.pop('')
    except KeyError:
        # debug print
        print("no non links found")

    return different_links_dic


# shows the amount of right and left news sites
# in a bar plot
# using a rated datasets of different news sites
def show_bias_of_news_links(news_urls, name):
    media_name_array = pd.read_csv("../../data/all_sides_news_merged.csv")

    # stelle 0 = allsides
    # stelle 1 = left
    # stelle 2 = left center
    # stelle 3 = center
    # stelle 4 = right center
    # stelle 5 = right
    counts = [0, 0, 0, 0, 0, 0]
    # found = 0
    for index, y in media_name_array.iterrows():
        z = y['news_url']
        if z == "NaN":
            continue
        if len(str(z).split('//')) - 1 > 0:
            netloc_to_look = str(z).split('//')[1]
        else:
            netloc_to_look = str(z)

        if len(netloc_to_look.split('www.')) - 1 > 0:
            netloc_to_look = netloc_to_look.split('www.')[1]
        else:
            netloc_to_look = netloc_to_look

        netloc_to_look = netloc_to_look.split('/')[0]

        for media, value in news_urls:
            if len(str(media).split('//')) - 1 > 0:
                netloc_check = str(media).split('//')[1]
            else:
                netloc_check = str(media)

            if len(netloc_check.split('www.')) - 1 > 0:
                netloc_check = netloc_check.split('www.')[1]
            else:
                netloc_check = netloc_check

            netloc_check = netloc_check.split('/')[0]

            if netloc_to_look == netloc_check:
                # found = 1
                rating = y['rating_num']
                if rating == 1:  # left
                    counts[1] = counts[1] + 1
                elif rating == 2:  # left center
                    counts[2] = counts[2] + 1
                elif rating == 3:  # center
                    counts[3] = counts[3] + 1
                elif rating == 4:  # right center
                    counts[4] = counts[4] + 1
                elif rating == 5:  # right
                    counts[5] = counts[5] + 1
                else:  # allsides
                    counts[0] = counts[0] + 1

                break
        # Debug print
        # if found == 0:
        #     print(str(index) + " : " + netloc_to_look)
        # else:
        #     found = 0

    print("[MESSAGE] Out of " + str(len(news_urls)) + " news links we have " +
          str(counts[1] + counts[2] + counts[3] + counts[4] + counts[5]) +
          " in our ranked dataset with the following leaning")
    plt.title(name)
    plt.bar(["Left", "Left center", "Center", "Right center", "Right"], height=counts[1:],
            color=["#0000C0", "blue", "purple", "red", "#C00000"])
    plt.show()
    print("[PLOT] plotted bar chart with leaning of links")


def get_links_of_all_sides_dataset(link_allsides):
    response = requests.get(link_allsides)

    page2 = BeautifulSoup(response.content)
    if not page2.find('div', class_='dynamic-grid'):
        print("NaN")
        return "NaN"

    content_link = []
    counter = 0
    for a in page2.find('div', class_='dynamic-grid'):
        if counter == 1:
            content_link = str(a)
            break
        counter = counter + 1

    start_link = content_link.find("a href")
    start_quote = content_link.find('"', start_link)
    end_quote = content_link.find('"', start_quote + 1)
    url = content_link[start_quote + 1: end_quote]
    print(url)
    return url


if __name__ == "__main__":
    # -------------- Do not use this anymore data got edited to new csv file.
    # all_sides = pd.read_csv("../data/allsides_data.csv")
    # all_sides['news_url'] = [get_links_of_all_sides_dataset(x) for x in all_sides['url']]
    # all_sides.to_csv("../data/all_sides_news_indexed.csv", index=False)

    print("-----------------------------------")
    print("[Start] Europe Data")
    submissions = pd.read_csv("../../data/Europe/Europe_submissions_authors_labeled", error_bad_lines=False)
    all_links = submissions["url"]

    link_dictionary = extract_different_links(all_links)
    create_pie_chart(link_dictionary, submissions.shape[0], 50, "Europe")

    print("[End] Europe Data")
    print("-----------------------------------")

    print("-----------------------------------")
    print("[Start] Democrats Data")
    submissions = pd.read_csv("../../data/Democrats/Democrats_submissions_authors_labeled", error_bad_lines=False)
    all_links = submissions["url"]

    link_dictionary = extract_different_links(all_links)
    create_pie_chart(link_dictionary, submissions.shape[0], 65, "Democrats")
    #
    print("[End] Democrats Data")
    print("-----------------------------------")

    print("-----------------------------------")
    print("[Start] Republican Data")
    submissions = pd.read_csv("../../data/Republican/Republican_submissions_authors_labeled", error_bad_lines=False)
    all_links = submissions["url"]

    link_dictionary = extract_different_links(all_links)
    create_pie_chart(link_dictionary, submissions.shape[0], 60, "Republican")

    print("[End] Republican Data")
    print("-----------------------------------")

    print("-----------------------------------")
    print("[Start] Conservative Data")
    submissions = pd.read_csv("../../data/Conservative/Conservative_submissions_authors_labeled", error_bad_lines=False)
    all_links = submissions["url"]

    link_dictionary = extract_different_links(all_links)
    create_pie_chart(link_dictionary, submissions.shape[0], 40, "Conservative")

    print("[End] Conservative Data")
    print("-----------------------------------")

    print("[Finish]")
