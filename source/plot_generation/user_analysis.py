import pandas as pd
import plotly.express as px
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np


def get_activity_num(activity, collumn_name, pos_neg, cut):
    activity_return = activity['author'].value_counts().to_frame()
    activity_return = activity_return.loc[lambda df: df['author'] > cut]
    activity_return = pd.DataFrame(activity_return).reset_index()
    activity_return.columns = ['author', collumn_name]
    activity_return = activity_return.loc[lambda df: df['author'] != 'AutoModerator']

    author_comments_pos_neg = activity.value_counts(["author", "sentiment_label_binary"]).to_frame()
    author_comments_pos_neg = pd.DataFrame(author_comments_pos_neg).reset_index()
    author_comments_pos = author_comments_pos_neg[author_comments_pos_neg['sentiment_label_binary'] == True]
    author_comments_neg = author_comments_pos_neg[author_comments_pos_neg['sentiment_label_binary'] == False]
    author_comments_pos = author_comments_pos.drop('sentiment_label_binary', 1)
    author_comments_neg = author_comments_neg.drop('sentiment_label_binary', 1)

    activity_return = pd.merge(activity_return, author_comments_pos, how="left", on=["author"])
    activity_return = pd.merge(activity_return, author_comments_neg, how="left", on=["author"])
    activity_return.columns = ['author', collumn_name, pos_neg[0], pos_neg[1]]
    activity_return = activity_return.fillna(0)
    activity_return[pos_neg[0]] = activity_return[pos_neg[0]].astype(int)
    activity_return[pos_neg[1]] = activity_return[pos_neg[1]].astype(int)

    author_score = activity[['author', 'score']].groupby('author').mean()
    activity_return = pd.merge(activity_return, author_score, how="left", on=["author"])
    return activity_return


# Helper Functions
def get_comments_and_subs_per_author(submissions_, comments_, submission_cut=0, comment_cut=0):
    # get amount of comments per user
    author_comments_ = get_activity_num(comments_, 'comments', ['pos_c', 'neg_c'], comment_cut)

    # get amount of submissions per user
    author_submissions_ = get_activity_num(submissions_, 'submissions', ['pos_s', 'neg_s'], submission_cut)

    # merge
    author = pd.merge(author_submissions_, author_comments_, how="outer", on=["author"])
    author = author.fillna(0)
    author['submissions'] = author['submissions'].astype(int)
    author['comments'] = author['comments'].astype(int)
    author['pos_s'] = author['pos_s'].astype(int)
    author['neg_s'] = author['neg_s'].astype(int)
    author['pos_c'] = author['pos_c'].astype(int)
    author['neg_c'] = author['neg_c'].astype(int)

    author["sum"] = author["submissions"] + author["comments"]
    author["pos_ratio"] = (author["pos_s"] + author["pos_c"]) / author["sum"] * 100
    author["neg_ratio"] = (author["neg_s"] + author["neg_c"]) / author["sum"] * 100
    author["pos_sum"] = author["pos_s"] + author["pos_c"]
    author["neg_sum"] = author["neg_s"] + author["neg_c"]

    author = author.sort_values(by=['sum'], ascending=False)
    return author, author_submissions_, author_comments_


def get_posts_of_top_authors(authors, data_to_search, num_of_authors):
    df_authors_posts = []
    for i in range(num_of_authors):
        current_author = authors.iloc[i].author
        df_authors_posts.append(data_to_search[data_to_search['author'] == current_author])
    return df_authors_posts


def get_num_of_activity_per_day(start, end, activity):
    date_range = []
    current_date = start

    cumm_activity = []
    for i in activity:
        cumm_activity.append([i, 0])

    while current_date < end:
        for auth_comm_ in cumm_activity:
            auth_comm = auth_comm_[0]
            array_to_append = []
            auth_comm = auth_comm.sort_values(by=['date'])

            array_to_append.append(int(current_date.timestamp()))  # time
            array_to_append.append(current_date.strftime("%m - %d"))  # time string
            array_to_append.append(auth_comm['author'].iloc[0])  # add author_name

            auth_comm = auth_comm.loc[lambda df: df['date'] > int(current_date.timestamp())]
            auth_comm = auth_comm.loc[lambda df: df['date'] < int((current_date + dt.timedelta(1)).timestamp())]

            array_to_append.append(len(auth_comm.index))  # num of comments
            auth_comm_[1] = auth_comm_[1] + len(auth_comm.index)
            array_to_append.append(auth_comm_[1])
            date_range.append(array_to_append)
        current_date = current_date + dt.timedelta(1)

    return pd.DataFrame(date_range, columns=["utc", "time", "author", "num_of_com", "cum"])


def get_sentiment_boxes(author_sum, cut_upper, cut_lower, only_one=False):
    if cut_upper != -1:
        author_sum = author_sum.loc[lambda df: df['sum'] < cut_upper]
    if cut_lower != -1:
        author_sum = author_sum.loc[lambda df: df['sum'] > cut_lower]

    if only_one:
        author_sum = author_sum.loc[lambda df: df['sum'] == 1]

    sentiment_boxes = []
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 90].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 80].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 70].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 60].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 50].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 40].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 30].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 20].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 10].count()[0])
    sentiment_boxes.append(author_sum[author_sum['pos_ratio'] >= 0].count()[0])
    minus = sentiment_boxes[0]
    sentiment_boxes[1] = sentiment_boxes[1] - minus
    minus = minus + sentiment_boxes[1]
    sentiment_boxes[2] = sentiment_boxes[2] - minus
    minus = minus + sentiment_boxes[2]
    sentiment_boxes[3] = sentiment_boxes[3] - minus
    minus = minus + sentiment_boxes[3]
    sentiment_boxes[4] = sentiment_boxes[4] - minus
    minus = minus + sentiment_boxes[4]
    sentiment_boxes[5] = sentiment_boxes[5] - minus
    minus = minus + sentiment_boxes[5]
    sentiment_boxes[6] = sentiment_boxes[6] - minus
    minus = minus + sentiment_boxes[6]
    sentiment_boxes[7] = sentiment_boxes[7] - minus
    minus = minus + sentiment_boxes[7]
    sentiment_boxes[8] = sentiment_boxes[8] - minus
    minus = minus + sentiment_boxes[8]
    sentiment_boxes[9] = sentiment_boxes[9] - minus
    return sentiment_boxes


# Plot Functions
def plot_top_author_activity(sub_reddit_name, author_sum_, author_submissions_, author_comments_, submissions_per_day_,
                             comments_per_day_, top_author_sentiment_binary_, average_score, head_=5):

    px.bar(author_sum_.head(head_), x="author", y=["comments", "submissions"],
           title=sub_reddit_name + ": Authors with most activity").show()
    px.bar(author_submissions_.head(head_), x="author", y="submissions",
           title=sub_reddit_name + ": Authors with most submissions").show()
    px.bar(author_comments_.head(head_), x="author", y="comments",
           title=sub_reddit_name + ": Authors with most comments").show()

    chart = px.bar(author_comments_.head(head_), x="author", y="score",
           title=sub_reddit_name + ": Authors with most comments show average score")
    chart.add_hline(average_score, annotation_text="average score of subreddit", line_width=2, line_dash="dash",
                    line_color="red")
    chart.show()

    px.bar(submissions_per_day_, x="time", y="num_of_com", color="author",
           title=sub_reddit_name + ": Submissions per day").show()
    px.bar(comments_per_day_, x="time", y="num_of_com", color="author",
           title=sub_reddit_name + ": Comments per day").show()
    #px.area(submissions_per_day_, x="time", y="cum", color="author", title=sub_reddit_name + ": Submissions over time").show()
    px.area(comments_per_day_, x="time", y="cum", color="author", title=sub_reddit_name + ": Comments over time").show()

    # sentiment
    px.bar(author_submissions_.head(head_), x="author", y=[author_submissions_.head(head_)["pos_s"],
                                                           author_submissions_.head(head_)["neg_s"] * -1],
           title=sub_reddit_name + ": Top author submissions with pos/neg sentiment", labels=["pos", "neg"]).show()
    px.bar(author_comments_.head(head_), x="author", y=[author_comments_.head(head_)["pos_c"],
                                                        author_comments_.head(head_)["neg_c"] * -1],
           title=sub_reddit_name + ": Top author comments with pos/neg sentiment", labels=["pos", "neg"]).show()

    top_author_sentiment_binary_ = top_author_sentiment_binary_.sort_values(by=['pos_ratio'], ascending=False)
    px.bar(top_author_sentiment_binary_.head(head_), x="author",
           y=[top_author_sentiment_binary_.head(head_)["pos_ratio"],
              top_author_sentiment_binary_.head(head_)["neg_ratio"]],
           title=sub_reddit_name + ": Authors with the most positive sentiment (and more than 20 comments)",
           labels=["pos", "neg"]).show()

    top_author_sentiment_binary_ = top_author_sentiment_binary_.sort_values(by=['pos_ratio'], ascending=True)
    px.bar(top_author_sentiment_binary_.head(head_), x="author",
           y=[top_author_sentiment_binary_.head(head_)["pos_ratio"],
              top_author_sentiment_binary_.head(head_)["neg_ratio"]],
           title=sub_reddit_name + ": Authors with the most negative sentiment (and more than 20 comments)",
           labels=["pos", "neg"]).show()

    sentiment_boxes = get_sentiment_boxes(author_sum_, 10, 2)
    plt.bar([">90%", ">80%", ">70%", ">60%", ">50%", ">40%", ">30%", ">20%", ">10%", ">0%"],
            height=sentiment_boxes)
    plt.title(sub_reddit_name + ": Upper Cut 10")
    plt.show()

    sentiment_boxes = get_sentiment_boxes(author_sum_, -1, 10)
    plt.bar([">90%", ">80%", ">70%", ">60%", ">50%", ">40%", ">30%", ">20%", ">10%", ">0%"],
            height=sentiment_boxes)
    plt.title(sub_reddit_name + ": Lower Cut 10")
    plt.show()

    sentiment_boxes = get_sentiment_boxes(author_sum_, -1, 20)
    plt.bar([">90%", ">80%", ">70%", ">60%", ">50%", ">40%", ">30%", ">20%", ">10%", ">0%"],
            height=sentiment_boxes)
    plt.title(sub_reddit_name + ": Lower Cut 20")
    plt.show()

    sentiment_boxes = get_sentiment_boxes(author_sum_, -1, 50)
    plt.bar([">90%", ">80%", ">70%", ">60%", ">50%", ">40%", ">30%", ">20%", ">10%", ">0%"],
            height=sentiment_boxes)
    plt.title(sub_reddit_name + ": Lower Cut 50")
    plt.show()

    sentiment_boxes = get_sentiment_boxes(author_sum_, -1, 100)
    plt.bar([">90%", ">80%", ">70%", ">60%", ">50%", ">40%", ">30%", ">20%", ">10%", ">0%"],
            height=sentiment_boxes)
    plt.title(sub_reddit_name + ": Lower Cut 100")
    plt.show()


def analyse_user_one_sub_reddit(submissions_link, comments_link, sub_reddit_name, head=50):
    # TODO: check if top user have many upvotes
    start_date = dt.datetime(2020, 10, 10)
    end_date = dt.datetime(2020, 11, 25)
    submissions = pd.read_csv(submissions_link, error_bad_lines=False)
    comments = pd.read_csv(comments_link, error_bad_lines=False)

    # get top authors on activity
    author_sum, author_submissions, author_comments = get_comments_and_subs_per_author(submissions, comments)
    top_author_comments = get_posts_of_top_authors(author_comments, comments, head)
    top_author_submissions = get_posts_of_top_authors(author_submissions, submissions, head)

    # get top authors on sentiment
    top_author_sentiment_binary = author_sum.loc[lambda df: df['sum'] > 15]

    # get activity from top authors per day
    submissions_per_day = get_num_of_activity_per_day(start_date, end_date, top_author_submissions)
    comments_per_day = get_num_of_activity_per_day(start_date, end_date, top_author_comments)

    # plot
    plot_top_author_activity(sub_reddit_name, author_sum, author_submissions, author_comments, submissions_per_day,
                             comments_per_day, top_author_sentiment_binary, comments['score'].mean(), head)


def analyse_user_multiple_sub_reddit(links, number_of_needed_subreddits=2, sum_cut=0, plot_number=10):
    # TODO: maybe add number_of_needed_subreddits
    activity = []
    authors_ = []
    for link in links:
        submission = pd.read_csv(link[0], error_bad_lines=False)
        comment = pd.read_csv(link[1], error_bad_lines=False)

        author_sum, author_submissions, author_comments = get_comments_and_subs_per_author(submission, comment)
        author_sum = author_sum.loc[lambda df: df['sum'] > sum_cut]
        activity.append(author_sum)
        authors = author_sum['author'].tolist()
        authors_.append(authors)

    merge = set(authors_[0])
    for auth in authors_:
        merge = merge & set(auth)
    merged_authors = list(merge)
    if len(merged_authors) == 0:
        print("Could not find matching authors!")
        return False

    only_real_authors = []
    for acti in activity:
        new_df = acti[acti['author'].isin(merged_authors)]
        new_df = new_df.sort_values(by=['author'], ascending=False)
        only_real_authors.append(new_df)

    merged_authors_df = pd.DataFrame()
    merged_authors_df['author'] = only_real_authors[0]['author'].values
    merged_authors_df['sum'] = only_real_authors[0]['sum'].values
    merged_authors_df['pos'] = only_real_authors[0]['pos_s'].values + only_real_authors[0]['pos_c'].values
    merged_authors_df['neg'] = only_real_authors[0]['neg_s'].values + only_real_authors[0]['neg_c'].values

    for i, real_auth in enumerate(only_real_authors):
        if i == 0:
            merged_authors_df[links[0][2] + ' sum'] = real_auth['sum'].values
            continue
        merged_authors_df['sum'] = merged_authors_df['sum'].values + real_auth['sum'].values
        merged_authors_df['pos'] = merged_authors_df['pos'].values + real_auth['pos_s'].values + \
                                   real_auth['pos_c'].values
        merged_authors_df['neg'] = merged_authors_df['pos'].values + real_auth['neg_s'].values + \
                                   real_auth['neg_c'].values

        merged_authors_df[links[i][2] + ' sum'] = real_auth['sum'].values

    merged_authors_df = merged_authors_df.sort_values(by=['sum'], ascending=False)
    y_list = []
    title = "Authors with most activity over Subreddits "
    for link in links:
        y_list.append(link[2] + ' sum')
        title = title + link[2] + ", "
    px.bar(merged_authors_df.head(plot_number), x="author", y=y_list,
           title=title,
           barmode='group').show()


if __name__ == "__main__":
    # TODO: check if normal republicans don't have comments between 11 - 17 november
    subreddits = ['Democrats', 'Republican', 'Conservative', 'Europe']

    #subreddits = ['Conservative']
    generated_links = []
    for sub in subreddits:
        generated_links.append(["data/" + sub + "/" + sub + "_submissions_authors_labeled",
                                "data/" + sub + "/" + sub + "_comments_authors_labeled",
                                sub])

    for i, sub in enumerate(generated_links):
        if i > 0:
            pass
            #continue
        analyse_user_one_sub_reddit(sub[0], sub[1], sub[2], 20)

    for i, sub in enumerate(generated_links):
        for j, sub2 in enumerate(generated_links):
            if i == j:
                continue
            analyse_user_multiple_sub_reddit([sub, sub2], 2, 0, 15)

    analyse_user_multiple_sub_reddit(generated_links[:3], 2, 0, 15)
    analyse_user_multiple_sub_reddit(generated_links, 2, 0, 15)

