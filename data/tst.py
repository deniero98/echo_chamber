import pandas as pd
import re

def remove_stuff1():
    f = open("data/submissions_republican_1598911200_1612134000_labeled_test.csv", "r")
    output = open("data/out.csv", "w")

    for line in f.readlines():
        matches = re.findall("^[0-9]+,[0-9]+", line)
        if not matches:
            output.write(line)
        else:
            print(line)

    f.close()
    output.close()

def remove_stuff2():
    f = open("data/comments_republican_1598911200_1612134000_labeled_bak.csv", "r")
    output = open("data/comments_republican_1598911200_1612134000_labeled_fixed.csv", "w")
    i = 0
    import pandas as pd
    fin = False
    while not fin:
        try:
            df = pd.read_csv("data/comments_republican_1598911200_1612134000_labeled_bak.csv")
        except Exception as e:
            subs = str(e)
            end = subs.index(",")
            start = subs.index("line")
            index = subs[start+5:end]
            for idx, line in enumerate(f.readlines()):
                idx+=1
                if idx == index:
                    print(f"dropping {idx}, {line}")
                else:
                    output.write(line)
            output.close()
            f.close()
            fin = True

if __name__ == "__main__":
    remove_stuff2()