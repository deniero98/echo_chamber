import requests
import requests.auth
import pandas as pd
import praw

def set_auth(bot_number:int=0):
    import requests.auth
    import pandas as pd
    import os
    data = pd.read_csv("clients.txt").to_dict()
    client_auth = requests.auth.HTTPBasicAuth(data["script"][bot_number], data["secret"][bot_number])
    post_data = {"grant_type": "password", "username": data["user"][bot_number], "password": data["passwd"][bot_number]}
    headers = {"User-Agent": "ChangeMeClient/0.1 by YourUsername"}
    response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data,
                             headers=headers)
    ret = response.json()
    print("client_id", data["script"][bot_number])
    print("client_secret", data["secret"][bot_number])
    print("user_agent", ret["access_token"])
    os.environ["REDDIT_CLIENT_ID"] = data["script"][bot_number]
    os.environ["REDDIT_CLIENT_SECRET"] = data["secret"][bot_number]
    os.environ["REDDIT_USER_AGENT"] = ret["access_token"]



def get_auth(bot_number:int=0) -> praw.Reddit:
    data = pd.read_csv("clients.txt").to_dict()

    client_auth = requests.auth.HTTPBasicAuth(data["script"][bot_number], data["secret"][bot_number])
    post_data = {"grant_type": "password", "username": data["user"][bot_number], "password": data["passwd"][bot_number]}
    headers = {"User-Agent": "ChangeMeClient/0.1 by YourUsername"}
    response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data,
                             headers=headers)
    ret = response.json()
    print("client_id", data["script"][bot_number])
    print("client_secret", data["secret"][bot_number])
    print("user_agent", ret["access_token"])
    client = praw.Reddit(
        client_id=data["script"][bot_number],
        client_secret=data["secret"][bot_number],
        user_agent=ret["access_token"])
    print("read_only: ", client.read_only)
    return client


if __name__ == "__main__":
    set_auth(1)
    import os

    for k, v in os.environ.items():
        print(f'{k}={v}')