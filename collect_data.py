import sys
import datetime as dt
from psaw import PushshiftAPI
from get_credentials import get_auth
import csv
import time
import collections
from concurrent.futures import ThreadPoolExecutor
from threading import Lock, Thread

apis = []

def authenticator_t(lock):
    print("starting auth")
    while True:
        time.sleep(10*60)
        with lock:
            global apis
            apis = [PushshiftAPI(get_auth(i)) for i in range(4)]
            print("reauthenticated accounts!")


def get_submissions(start, end, subreddit):
    file_start = start
    file_end = end
    executor = ThreadPoolExecutor(6)
    account_lock = Lock()
    global apis
    apis = [PushshiftAPI(get_auth(i)) for i in range(4)]
    thread = Thread(target=authenticator_t, args=(account_lock,))
    thread.daemon = True
    thread.start()

    start_time = time.time()

    header_sub = ['id', 'title', 'author', 'subreddit', 'body', 'ups', 'downs', 'upvote_ratio', 'shortlink', 'created_utc']
    header_top_com = ['sub_id', 'id', 'author', 'body', 'ups', 'downs', 'created_utc']
    data_sub = []
    lock = Lock()
    data_top_com = []
    current_date = start
    block_size = 500
    while True:
        print("New Loop with current Date: " + str(dt.datetime.fromtimestamp(current_date)))
        with account_lock:
            try:
                gen = apis[3].search_submissions(after=current_date,
                                             before=end,
                                             sort_type='created_utc',
                                             sort='asc',
                                             subreddit=subreddit,
                                             limit=block_size)
                results = list(gen)
            except Exception as e:
                print(str(e))
                apis[3] = PushshiftAPI(get_auth(3))
                gen = apis[3].search_submissions(after=current_date,
                                             before=end,
                                             sort_type='created_utc',
                                             sort='asc',
                                             subreddit=subreddit,
                                             limit=block_size)
                results = list(gen)

        futures = []
        for i, submission in enumerate(results):
            if i % 50 == 0:
                print("i: " + str(i) + " - " + str(dt.datetime.fromtimestamp(results[i].created_utc)))
            data_sub.append([submission.id, submission.title, submission.author, submission.subreddit, submission.ups,
                             submission.downs, submission.upvote_ratio, submission.shortlink, int(submission.created_utc)])

            api_idx = i % 3

            def get_comments(submission_id, nest_level, limit, api, index):
                if index % 50 == 0:
                    print(f"getting comments {index}")
                try:
                    # time.sleep(0.005)
                    comments = api.search_comments(link_id=submission_id, sort_type='score', sort='desc', nest_level=nest_level,
                                                   limit=limit)   # TODO: improve perf, more than 500
                    comment_list = []
                    for comment in comments:
                        if not hasattr(comment, 'author'):
                            #print("Could not find author")
                            continue
                        if comment.stickied:
                            #print("Comment is stickied")
                            continue
                        comment_list.append([submission.id, comment.id, comment.author, str(comment.body).replace('\n', ' '),
                                             comment.ups, comment.downs, int(comment.created_utc)])
                    with lock:
                        data_top_com.extend(comment_list)
                    # print(f"got future of {submission_id}, limit {limit}, nest_level {nest_level}")
                    if index % 50 == 0:
                        print(f"got comments {index}")
                except Exception as e:
                    print(f"failed future of {submission_id}, limit {limit}, nest_level {nest_level}")
                    print(str(3))

                return
            with account_lock:
                future = executor.submit(get_comments, submission.id, 1, 500, apis[api_idx], i)
            futures.append(future)

        finished = False
        while not finished:
            time.sleep(1)
            finished = True
            for f in futures:
                if f.done() == False:
                    finished = False

        current_date = int(results[-1].created_utc) + 1         # TODO maybe duplicate
        if len(results) < block_size:
            break

    with open(f'submissions_{subreddit}_{str(file_start)}_{str(file_end)}.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header_sub)
        writer.writerows(data_sub)

    with open(f'comments_{subreddit}_{str(file_start)}_{str(file_end)}.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header_top_com)
        writer.writerows(data_top_com)

    print("Time elapsed: " + str(time.time() - start_time))
    sys.exit()


if __name__ == "__main__":
    start = int(dt.datetime(2020, 9, 1).timestamp())
    end = int(dt.datetime(2021, 2, 1).timestamp())
    get_submissions(start, end, 'conservative')

    # republican - conservative - europe - politics - republicans
